#!/bin/bash

function flashTerm {
  while [ 1 -eq 1 ]
  do
    read -n 1 -s -t 1
    if [ $? -eq 142 ]
    then
      printf '\e[?5h'
    else
      return
    fi

    read -n 1 -s -t 1
    res=$?
    printf '\e[?5l'
    if [ $res -eq 0 ]
    then
      return
    fi
  done
}

# Get specified time to wait for, in seconds
if [ $# -ge 1 ]
then
  timeLim=$1
else
  # timeout in sec: 60 * 30 = 1800 (half an hour)
  timeLim=1800
fi

while [ 1 -eq 1 ]
do
  read -n 1 -s -t $timeLim
  if [ $? -eq 142 ]
  then
    flashTerm
  fi
done
